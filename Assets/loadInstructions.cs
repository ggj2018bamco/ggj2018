﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class loadInstructions : MonoBehaviour, IPointerClickHandler  {

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        SceneManager.LoadScene("Instrucciones");

    }
}
