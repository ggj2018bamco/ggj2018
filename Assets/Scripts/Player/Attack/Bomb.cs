﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;
using System;

public class Bomb : MonoBehaviour {

    [SerializeField]
    private Rigidbody2D b_Rig;
    [SerializeField]
    private float b_Force;
    [SerializeField]
    private AudioClip[] clip;
    [SerializeField]
    private AudioSource source;
    [SerializeField]
    private UnityArmatureComponent b_Arm;
    [SerializeField]
    private CircleCollider2D explosion;

    public void StartBomb(bool direction) {
        b_Rig.AddForce(new Vector2( 1 * (direction ? -1 : 1), 1) * b_Force);
        source.PlayOneShot(clip[0]);
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        source.PlayOneShot(clip[1]);
        Debug.Log("Baaamb!");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Base" || collision.gameObject.tag == "Enemy")
        {
            source.PlayOneShot(clip[1]);
            Debug.Log("TBaaamb!");
            b_Rig.gravityScale = 0;
            b_Rig.velocity = Vector2.zero;
            StartCoroutine(Booom());
        }
    }

    private IEnumerator Booom()
    {
        b_Arm.animation.Play("Booom");
        explosion.enabled = true;
        yield return new WaitForSeconds(b_Arm.animation.lastAnimationState.totalTime);
        Destroy(gameObject);
    }
}
