﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;
using System;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

    [SerializeField]
    private Rigidbody2D p_Rig;
    [SerializeField]
    private float m_Speed, m_JumpForce;
    public int m_Jump, m_JumpCharges = 2;
    [SerializeField]
    private GameObject p_Bomb;
    [SerializeField]
    private UnityArmatureComponent p_Arm;
    [SerializeField]
    private BoxCollider2D c_Attack;
    [SerializeField]
    private CircleCollider2D c_Shield;
    [SerializeField]
    private Game_Manager manager;
    [SerializeField]
    private Audio_Manager audio_manager;
    private int currentAudio;

    [SerializeField]
    private AudioClip[] AudioClips;
    [SerializeField]
    private AudioSource m_audiosource;
    [SerializeField]
    private HUD_Manager hud_manager;

    private bool takingTotem, totemReady, alive, hurt;
    private GameObject currentTotem;
    private int lives = 3;
    private bool s_bomb, s_sword;

    public bool GetStatus
    {
        get { return alive; }
    }

    void Start() {
        m_Jump = m_JumpCharges;
        takingTotem = false;
        totemReady = false;
        s_bomb = true;
        s_sword = true;
        lives = 3;
        alive = true;
        hurt = false;
        currentAudio = 1;
        audio_manager.ChangeAudio(currentAudio);
        p_Arm.animation.timeScale = 1;
    }

    public void CodeFPS()
    {
        if (!takingTotem && alive)
        {
            if (Input.GetKey(KeyCode.RightArrow))
                p_Rig.velocity = new Vector2(transform.right.x * m_Speed, p_Rig.velocity.y);
            if (Input.GetKey(KeyCode.LeftArrow))
                p_Rig.velocity = new Vector2(transform.right.x * -m_Speed, p_Rig.velocity.y);
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                p_Arm.armature.flipX = true;
                c_Attack.transform.localPosition = new Vector2(3.0f, 2.0f);
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                p_Arm.armature.flipX = false;
                c_Attack.transform.localPosition = new Vector2(-3.0f, 2.0f);
            }
            if (Input.GetKeyDown(KeyCode.Space) && m_Jump > 0)
            {
                p_Rig.AddForce(transform.up * m_JumpForce);
                if (m_Jump == 2)
                {
                    //p_Arm.animation.GotoAndStopByProgress("Jump1_up", 0.9f);
                    m_audiosource.PlayOneShot(AudioClips[0]);
                }
                else
                {
                    p_Arm.animation.Play("Jump2_noheight"); //, 0.1f);
                    m_audiosource.PlayOneShot(AudioClips[1]);
                }
                m_Jump--;
            }
            if (Input.GetKeyDown(KeyCode.S) && s_bomb)
            {
                GameObject bomb = Instantiate(p_Bomb, new Vector2(transform.position.x, transform.position.y + 0.5f), Quaternion.identity, null);
                bomb.GetComponent<Bomb>().StartBomb(!p_Arm.armature.flipX);
            }
            if (Input.GetKeyDown(KeyCode.A) && p_Arm.animation.lastAnimationName != "Attack" && s_sword)
                StartCoroutine(Attack());
            if (p_Arm.animation.lastAnimationName != "Jump1_up" && m_Jump == m_JumpCharges && p_Arm.animation.lastAnimationName != "Attack")
            {
                if (Mathf.Abs(p_Rig.velocity.x) > 0 && p_Arm.animation.lastAnimationName != "Run")
                    p_Arm.animation.Play("Run"); //, 0.2f);
                else if (p_Rig.velocity.x == 0 && p_Arm.animation.lastAnimationName != "Idle")
                    p_Arm.animation.Play("Idle"); //, 0.2f);
            }
            //Debug.Log("Animation " + p_Arm.animation.lastAnimationName);
            if (Input.GetKeyDown(KeyCode.Q) && totemReady)
                StartCoroutine(CaptureTotem());
        }
    }

    private IEnumerator Attack()
    {
        p_Arm.animation.Play("Attack");
        m_audiosource.PlayOneShot(AudioClips[2]);

        c_Attack.enabled = true;
        yield return new WaitForSeconds(p_Arm.animation.lastAnimationState.totalTime / 2);
        c_Attack.enabled = false;
        yield return new WaitForSeconds(p_Arm.animation.lastAnimationState.totalTime / 2);
        p_Arm.animation.Play("Idle"); //, 0.1f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Base" && p_Arm.animation.lastAnimationName != "Run")
        {
            StartCoroutine(FinishJump());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        switch (collision.name)
        {
            case "Totem_1":
            totemReady = false;
                break;
            case "Totem_2":
                totemReady = false;
                break;
            case "Totem_3":
                totemReady = false;
                break;
            case "Totem_4":
                totemReady = false;
                break;
        }
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (alive)
        {
            switch (collision.name)
            {
                case "Totem_1":
                    totemReady = true;
                    currentTotem = collision.gameObject;
                    break;
                case "Totem_2":
                    totemReady = true;
                    currentTotem = collision.gameObject;
                    break;
                case "Totem_3":
                    totemReady = true;
                    currentTotem = collision.gameObject;
                    break;
                case "Totem_4":
                    totemReady = true;
                    currentTotem = collision.gameObject;
                    break;
                case "To_L2":
                    manager.Next_Level("L2");
                    break;
                case "To_L3":
                    manager.Next_Level("L3");
                    break;
                case "To_L4":
                    manager.Next_Level("L4");
                    break;
                case "To_L5":
                    SelectScene();
                    break;
                case "Attack_1":
                    if (!hurt)
                    {
                        hurt = true;
                        StartCoroutine(Hurt());
                    }
                    break;
                case "Shuriken(Clone)":
                    if(!c_Shield.isActiveAndEnabled)
                        StartCoroutine(Hurt());
                    break;
            }
        }
    }

    private void SelectScene()
    {
        if (currentAudio == 1 || currentAudio == 2 || currentAudio == 3)
        {
            SceneManager.LoadScene(3);
        }
        else if (currentAudio == 4)
        {
            SceneManager.LoadScene(4);
        }else
        {
            SceneManager.LoadScene(0);
        }
    }

    private IEnumerator Hurt()
    {
        Debug.Log("Lives " + lives);
        if (p_Arm.animation.lastAnimationName != "Hurt")
        {
            Debug.Log("Auch! " + lives);
            if (lives > 0)
            {
                Debug.Log("Animation!");
                p_Arm.animation.Play("Hurt");
                lives--;
                hud_manager.ChangeHeart(false);
                yield return new WaitForSeconds(p_Arm.animation.lastAnimationState.totalTime);
                p_Arm.animation.Play("Idle");
            }
            else
            {
                p_Arm.animation.Play("Death");
                alive = false;
                yield return new WaitForSeconds(p_Arm.animation.lastAnimationState.totalTime - 0.05f);
                p_Arm.animation.timeScale = 0;
                SceneManager.LoadScene(0);
            }
        }
        hurt = false;
    }

    private IEnumerator CaptureTotem()
    {
        takingTotem = true;
        p_Arm.animation.Play("Rewind"); //, 0.2f);
        yield return new WaitForSeconds(2.0f);
        Debug.Log("Totem capture");
        currentTotem.GetComponent<BoxCollider2D>().enabled = false;
        RemoveSkill(currentTotem.name);
        takingTotem = false;
    }

    private void RemoveSkill(string totem)
    {
        switch (totem)
        {
            case "Totem_1":
                m_JumpCharges = 1;
                hud_manager.LosePower(0);
                currentAudio++;
                Debug.Log("Lose DJ");
                break;
            case "Totem_2":
                c_Shield.enabled = false;
                hud_manager.LosePower(1);
                currentAudio++;
                Debug.Log("Lose shield");
                break;
            case "Totem_3":
                s_bomb = false;
                hud_manager.LosePower(2);
                currentAudio++;
                Debug.Log("Lose bomb");
                break;
            case "Totem_4":
                s_sword = false;
                hud_manager.LosePower(3);
                currentAudio++;
                Debug.Log("Lose sword");
                break;
        }
        audio_manager.ChangeAudio(currentAudio);
    }

    private IEnumerator FinishJump()
    {
        p_Arm.animation.Play("Jump1_down");
        yield return new WaitForSeconds(p_Arm.animation.lastAnimationState.totalTime / 2);
        p_Arm.animation.Play("Idle"); //, 0.2f);
        m_Jump = m_JumpCharges;
    }
}
