﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBarController : MonoBehaviour{ 

    [SerializeField]
    private GameObject rectGO;
    private SpriteRenderer renderTitle;

    private void Start()
    {
        //render = rectGO.GetComponent<SpriteRenderer>();
        renderTitle = gameObject.GetComponent<SpriteRenderer>();

    }

    private void OnMouseEnter()
    {
        rectGO.SetActive(true);
        renderTitle.color = new Color(44f, 1f, 73f);
    }

    private void OnMouseExit()
    {
        renderTitle.color = new Color(241f, 4f, 120f);
        rectGO.SetActive(false);
        
    }
}
