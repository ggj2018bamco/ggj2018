﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;
using System;

public class Enemy_Movement : MonoBehaviour {

    [SerializeField]
    private float e_Speed = 1.0f, e_AttackDistance = 2.0f, e_VDistance = 50.0f;
    [SerializeField]
    private UnityArmatureComponent e_Arm;

    public float Get_VDistance
    {
        get { return e_VDistance; }
    }

    private void Start()
    {
        e_Arm.animation.Play("Idle");
    }

    public void StopMove()
    {
        e_Arm.animation.Play("Idle"); //, 0.2f);
    }

    public void Attack(float pPos)
    {
        switch(gameObject.name.Substring(0,3))
        {
            case "Sai":
                GetComponent<Enemy_S>().StartAttack(pPos);
                break;
            case "Enemy_M":

                break;
            case "Shu":
                GetComponent<Enemy_L>().StartThrowing(pPos);
                break;
        }
    }

    public void StopAttack()
    {
        switch (gameObject.name.Substring(0, 3))
        {
            case "Sai":
                GetComponent<Enemy_S>().StopAttack();
                break;
            case "Enemy_M":

                break;
            case "Shu":
                Debug.Log("StartThrowing!");
                GetComponent<Enemy_L>().StopAttack();
                break;
        }
    }

    public void CodeFPS(Vector2 playerPos)
    {
        if (e_Arm.animation.lastAnimationName != "Walk" && !(Vector2.Distance(playerPos, transform.position) < e_AttackDistance))
        {
            e_Arm.animation.Play("Walk"); //, 0.2f);
            if (playerPos.x - transform.position.x < 0)
                e_Arm.armature.flipX = false;
            else
                e_Arm.armature.flipX = true;
        }
        if (Vector2.Distance(playerPos, transform.position) < e_AttackDistance)
            Attack(playerPos.x);
        else
            transform.position = new Vector2(transform.position.x - e_Speed * Time.deltaTime * ((transform.position.x - playerPos.x) < 0 ? -1 : 1), transform.position.y);
    }
}
