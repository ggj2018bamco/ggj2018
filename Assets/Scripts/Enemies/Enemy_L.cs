﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;

public class Enemy_L : MonoBehaviour {

	[SerializeField]
	private GameObject a_Shuriken;
	[SerializeField]
	private GameObject player;
	private GameObject shur;
    private bool e_Throwing;
    [SerializeField]
    private UnityArmatureComponent e_Arm;
    private int lives = 3;
    private Coroutine c_attacking;

    void Start()
    {
        e_Throwing = false;
        c_attacking = null;
    }

    public void StartThrowing(float pPos)
    {
        if (!e_Throwing)
        {
            e_Throwing = true;
            c_attacking = StartCoroutine(ThrowShurikens());
        }
    }

    public void StopAttack()
    {
        e_Throwing = false;
        if(c_attacking != null)
            StopCoroutine(c_attacking);
    }

    private IEnumerator ThrowShurikens()
    {
        Debug.Log("Throw!!");
        while (e_Throwing) {
            e_Arm.animation.Play("Attack"); //, 0.1f);
            yield return new WaitForSeconds(e_Arm.animation.lastAnimationState.totalTime * 4 / 5);
            shur = Instantiate(a_Shuriken, new Vector2(transform.position.x, transform.position.y + 0.5f), Quaternion.identity, null);
            Vector2 xypos = new Vector2((player.transform.position.x - shur.transform.position.x), (player.transform.position.y - shur.transform.position.y + 0.5f));
            shur.GetComponent<Shuriken>().SetXY = xypos.normalized;
            yield return new WaitForSeconds(e_Arm.animation.lastAnimationState.totalTime * 1 / 5);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (e_Arm.animation.lastAnimationName != "Hurt" && collision.name == "Damage")
        {
            lives--;
            if (lives > 0)
                StartCoroutine(HurtAnimation());
            else
                Destroy(gameObject);
        }
    }

    private IEnumerator HurtAnimation()
    {
        e_Arm.animation.Play("Hurt");
        yield return new WaitForSeconds(e_Arm.animation.lastAnimationState.totalTime);
        e_Arm.animation.Play("Idle");
    }
}