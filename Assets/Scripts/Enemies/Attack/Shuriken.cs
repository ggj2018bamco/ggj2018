﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shuriken : MonoBehaviour {

	private Vector2 xy;
    private Rigidbody2D rb;
    [SerializeField]
	private float waitTime= 0.01f;
    [SerializeField]
    private CircleCollider2D c_Shuriken;

	public Vector2 SetXY{
		set{ xy = value; MoveShuriken ();
		}
	}
		
	void MoveShuriken(){
		rb = GetComponent<Rigidbody2D>();
		rb.AddForce (xy * 5000);
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
        Debug.Log("Shuriken collider: " + collision.gameObject.name);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "Defense")
        {
            c_Shuriken.enabled = false;
            rb.gravityScale = 1;
            rb.AddForce(xy * -5700);
        }
        Debug.Log("Shuriken trigger: " + collision.gameObject.name);
    }
}
