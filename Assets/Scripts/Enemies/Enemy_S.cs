﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;

public class Enemy_S : MonoBehaviour {

    private Rigidbody2D e_Rig;
    private bool ground;
    private float e_Jump = 150;
    [SerializeField]
    private BoxCollider2D c_Attack1, c_Attack2;
    [SerializeField]
    private UnityArmatureComponent e_Arm;
    private bool attack = false;
    private Coroutine c_attacking;
    [SerializeField]
    private int lives = 3;

    void Start () {
        e_Rig = GetComponent<Rigidbody2D>();
        c_attacking = null;
        attack = false;
    }

    public void StartAttack(float pPos)
    {
        if (!attack)
        {
            attack = true;
            c_attacking = StartCoroutine(Attack());
        }
    }

    public void StopAttack()
    {
        attack = false;
        if (c_attacking == null)
            StopCoroutine(c_attacking);
    }

    private IEnumerator Attack()
    {
        while (attack)
        {
            e_Arm.animation.Play("Attack");
            yield return new WaitForSeconds(e_Arm.animation.lastAnimationState.totalTime * 2 / 3);
            c_Attack1.enabled = true;
            yield return new WaitForSeconds(e_Arm.animation.lastAnimationState.totalTime * 1 / 3);
            c_Attack1.enabled = false;
        }
        c_attacking = null;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Base")
        {
            ground = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Enemy trigger " + collision.name);
        if(e_Arm.animation.lastAnimationName != "Hurt" && collision.name == "Damage")
        {
            lives--;
            Debug.Log(lives);
            if (lives >= 0)
                StartCoroutine(HurtAnimation());
            else
                Destroy(gameObject);
        }
    }

    private IEnumerator HurtAnimation()
    {
        e_Arm.animation.Play("Hurt");
        yield return new WaitForSeconds(e_Arm.animation.lastAnimationState.totalTime);
    }
}
