﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD_Manager : MonoBehaviour {

    [SerializeField]
    private Image[] heart, power, vhs;
    [SerializeField]
    private Sprite[] brokenHeart, brokenPower;

    private int curr_Heart = 3;

    public void ChangeHeart(bool getHeart)
    {
        Debug.Log("# <3 : " + curr_Heart);
        if (getHeart)
        {
            if (curr_Heart < 3)
                curr_Heart++;
        }
        else
            curr_Heart--;
        switch (curr_Heart)
        {
            case 0:
                heart[0].sprite = brokenHeart[1];
                heart[1].sprite = brokenHeart[1];
                heart[2].sprite = brokenHeart[1];
                break;
            case 1:
                heart[0].sprite = brokenHeart[0];
                heart[1].sprite = brokenHeart[1];
                heart[2].sprite = brokenHeart[1];
                break;
            case 2:
                heart[0].sprite = brokenHeart[0];
                heart[1].sprite = brokenHeart[0];
                heart[2].sprite = brokenHeart[1];
                break;
            case 3:
                heart[0].sprite = brokenHeart[0];
                heart[1].sprite = brokenHeart[0];
                heart[2].sprite = brokenHeart[0];
                break;
        }
    }

    public void LosePower(int powerID)
    {
        switch (powerID)
        {
            case 0:
                power[0].sprite = brokenPower[0];
                vhs[0].enabled = true;
                break;
            case 1:
                power[1].sprite = brokenPower[1];
                vhs[1].enabled = true;
                break;
            case 2:
                power[2].sprite = brokenPower[2];
                vhs[2].enabled = true;
                break;
            case 3:
                power[3].sprite = brokenPower[3];
                vhs[3].enabled = true;
                break;
        }
    }
}
