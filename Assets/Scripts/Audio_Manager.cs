﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Audio_Manager : MonoBehaviour {

    [SerializeField]
    private AudioMixerSnapshot[] snapshot;


    public void ChangeAudio(int level)
    {
        switch(level)
        {
            case 1:
                snapshot[0].TransitionTo(0.5f);
                break;
            case 2:
                snapshot[1].TransitionTo(0.5f);
                break;
            case 3:
                snapshot[2].TransitionTo(0.5f);
                break;
            case 4:
                snapshot[3].TransitionTo(0.5f);
                break;
        }
    }
	
}
