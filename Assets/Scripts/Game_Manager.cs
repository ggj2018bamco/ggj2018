﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game_Manager : MonoBehaviour {

    [SerializeField]
    private List<GameObject> enemies_in, enemies_ac;
    private Enemy_S[] sc_enemyS;
    //private Enemy_M] sc_enemyM;
    //private Enemy_L[] sc_enemyL;
    [SerializeField]
    private GameObject player, mainCam;
    private Player sc_player;
    [SerializeField]
    private float e_VisionRange = 10.0f, camDistance = 50.0f;
    private bool gameActive;
    [SerializeField]
    private Transform[] origin;
    [SerializeField]
    private Image panel;

    private int s = 0, m = 0, l = 0;

	void Start () {
        s = 0; m = 0; l = 0;
        sc_player = player.GetComponent<Player>();
        //player.transform.position = origin[0].position;
    }

    private void Enemy_Distance()
    {
        for (int i = 0; i < enemies_in.Count; i++)
        {
            if (Vector2.Distance(player.transform.position, enemies_in[i].transform.position) < enemies_in[i].GetComponent<Enemy_Movement>().Get_VDistance)
            {
                if (enemies_in[i] != null)
                {
                    enemies_ac.Add(enemies_in[i]);
                    enemies_in.Remove(enemies_in[i]);
                }
            }
        }
        for (int i = 0; i < enemies_ac.Count; i++)
        {
            if (enemies_ac[i] != null)
            {
                if (Vector2.Distance(player.transform.position, enemies_ac[i].transform.position) > enemies_in[i].GetComponent<Enemy_Movement>().Get_VDistance)
                {
                    enemies_ac[i].GetComponent<Enemy_Movement>().StopMove();
                    enemies_ac[i].GetComponent<Enemy_Movement>().StopAttack();
                    enemies_in.Add(enemies_ac[i]);
                    enemies_ac.Remove(enemies_ac[i]);
                }
            }
        }
    }

    void Update () {
        sc_player.CodeFPS();
        if (sc_player.GetStatus)
        {
            for (int ac = 0; ac < enemies_ac.Count; ac++)
            {
                if (enemies_ac[ac] != null)
                    enemies_ac[ac].GetComponent<Enemy_Movement>().CodeFPS(player.transform.position);
            }
            Enemy_Distance();
        }
    }

    public void Next_Level(string level)
    {
        StartCoroutine(CamTransition(level));
    }

    private IEnumerator CamTransition(string next_level)
    {
        for(float i = 0; i < 1; i+=0.01f)
        {
            panel.color = new Color(0, 0, 0, i);
            yield return new WaitForSeconds(0.01f);
        }
        switch (next_level)
        {
            case "L2":
                player.transform.position = origin[1].position;
                break;
            case "L3":
                player.transform.position = origin[2].position;
                break;
            case "L4":
                player.transform.position = origin[3].position;
                break;
        }
        for (float i = 1; i > 0; i -= 0.01f)
        {
            panel.color = new Color(0, 0, 0, i);
            yield return new WaitForSeconds(0.01f);
        }
    }
}
