﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField]
    private GameObject player;
    [SerializeField]
    private Camera mainCAM;

    private Vector3 offset;
    float bordeLateral, bordeSuperior;
    [SerializeField]
    private GameObject fondoGO;

    private SpriteRenderer fondo;

    private Vector2 tamaño;

    private float cameraSize, cameraHorizontal, camareVertcal;

	// Use this for initialization
	void Start () {
        offset = transform.position - player.transform.position;
        fondo = fondoGO.GetComponent<SpriteRenderer>();
        tamaño = fondo.size;
        cameraSize = mainCAM.orthographicSize;
        Debug.Log(cameraSize);
    }
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = player.transform.position + offset;
        

        if (transform.position.x > tamaño.x)
        {
            bordeLateral = Mathf.Abs(transform.position.x) - tamaño.x;
            
            transform.position = player.transform.position + new Vector3(offset.x - bordeLateral, offset.y,offset.z);

            if(transform.position.y > 4)
            {
                bordeSuperior = Mathf.Abs(transform.position.y) - 4;
                transform.position = player.transform.position + new Vector3(offset.x, offset.y - bordeSuperior, offset.z);
            }
        }else if(transform.position.y > 4)
        {
            bordeSuperior = Mathf.Abs(transform.position.y) - 4;
            transform.position = player.transform.position + new Vector3(offset.x, offset.y - bordeSuperior, offset.z);

            if(transform.position.x > 7)
            {
                bordeLateral = Mathf.Abs(transform.position.x) - 7;

                transform.position = player.transform.position + new Vector3(offset.x - bordeLateral, offset.y, offset.z);
            }
        }
            
	}
}
